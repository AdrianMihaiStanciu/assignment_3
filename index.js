
const FIRST_NAME = "Adrian-Mihai";
const LAST_NAME = "Stanciu";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
class Employee {
    constructor (name, surname, salary){
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    getDetails() {
        return `${this.name} ${this.surname} ${this.salary}`;
    }
}

class SoftwareEngineer extends Employee {
   constructor (name, surname, salary, experience){
       super(name, surname, salary);
       if(typeof experience ===  "undefined"){
           this.experience = 'JUNIOR';
       }
       else{
           this.experience = experience;
       }
   }

   applyBonus(){
       if (this.experience == 'JUNIOR') return this.salary*1.1;
       if (this.experience == 'MIDDLE') return this.salary*1.15;
       if (this.experience == 'SENIOR') return this.salary*1.2;
       return this.salary*1.1;
   }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

